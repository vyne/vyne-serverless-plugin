import {expect} from "chai";
import {NameUtils} from "./names";

describe("NameUtils", () => {
   it("should remove dashes from names", () => {
      expect(NameUtils.makeSafe("foo-bar")).to.equal("foobar");
   })
});
