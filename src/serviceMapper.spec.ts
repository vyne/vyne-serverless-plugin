import {ServerlessOperationProviderFactory} from "./operationMapper";
import {schemaFromFile, SchemaGeneratorOptions} from "@taxilang/taxi-typescript";
import {ServerlessService} from "./types";
import {ServerlessServiceMapperFactory} from "./mapperFactories";
import * as fs from 'fs';
import {expect} from "chai";

describe("parsing aws handlers", () => {
   let src = `
import {APIGatewayEvent, Callback, Context, Handler} from 'aws-lambda';

interface Client {

}

const getClientByUserId: Handler = (event, context: Context, callback: Callback<Client>) => {
};
export {getClientByUserId}
`;
   let serverless: ServerlessService =
      {
         "serviceObject": {
            "name": "myAwsService"
         },
         custom: null,
         "provider": {
            name: "aws"
         },
         functions: {
            "getClient": {
               "handler": "test/demoHandler.getClientByUserId",
               "events": [
                  {
                     "http": {
                        "path": "hello/{name}", "method": "get", "pathVariableTypes": {"name": "ClientName"}
                     }
                  }]
            }
         },
         getAllFunctions: () => [],
         package: {
            include: [], exclude: []
         }
      };

   const options: SchemaGeneratorOptions = new SchemaGeneratorOptions()
      .prependOperationProviderFactory(new ServerlessOperationProviderFactory(serverless, "http://some.aws.service/"));
   options.serviceMapperFactory = new ServerlessServiceMapperFactory(serverless);
   // any is here b/c of an odd compiler issue... need to investigate.

   if (!fs.existsSync("test/demoHandler.ts")) {
      throw new Error("Invalid file")
   }
   let schema = schemaFromFile('test/demoHandler.ts', <any>options);

   it("should generate operations for functions declared in config", () => {
      let service = schema.service("myAwsService");
      expect(service.qualifiedName).to.equal("myAwsService");
      expect(service.operations).to.have.length(1);
      let operation = service.operations[0];

      expect(operation.annotations).to.have.length(1);
      let httpAnnotation = operation.annotations[0];
      expect(httpAnnotation.name).to.equal("HttpOperation");
      expect(httpAnnotation.parameters.method).to.equal("GET");
      expect(httpAnnotation.parameters.url).to.equal("http://some.aws.service/hello/{ClientName}")

   })
   // it("should detect return type from Handler interface", () => {
   //
   //    let operation = schema.operation("handlerWithReturnType");
   //    expect(operation.returnType.qualifiedName).to.equal("Client")
   // });

   // it("should set path from serverless config", () => {
   //
   // })
   //
   // it("should use Any as return type if not specified", () => {
   //    let schema = schemaFromFile('./test/awsServerlessModels.ts', null);
   //    let operation = schema.operation("handlerWithoutReturnType");
   //    expect(operation.returnType.qualifiedName).to.equal(Primitives.ANY.qualifiedName)
   // });
   // it("should add operations without a service to DefaultService", () => {
   //    let schema = schemaFromFile('./test/awsServerlessModels.ts');
   //    expect(schema.service("lang.taxi.DefaultService")).not.to.be.undefined
   // })
});
