import {
   OperationProviderFactory,
   ServiceGeneratorFactory,
   ServiceMapper,
   TypeHelper,
   TypeMapper
} from "@taxilang/taxi-typescript";
import {ServerlessService} from "./types";
import {ServerlessServiceMapper} from "./serviceMapper";

export class ServerlessServiceMapperFactory implements ServiceGeneratorFactory {
   constructor(private serverlessService: ServerlessService) {
   }

   build(typeHelper: TypeHelper, typeMapper: TypeMapper, operationFactory: OperationProviderFactory): ServiceMapper {
      return new ServerlessServiceMapper(this.serverlessService, typeHelper, typeMapper, operationFactory.build(typeHelper, typeMapper))
   }

}

