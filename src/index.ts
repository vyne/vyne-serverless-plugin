import {ServerlessFunction, ServerlessInstance, VynePluginConfig} from "./types";
import * as tsUtils from './typescript';
import {SchemaGenerator, SchemaGeneratorOptions, SchemaWriter} from "@taxilang/taxi-typescript";
import {ServerlessOperationProviderFactory} from "./operationMapper";
import {ServerlessServiceMapperFactory} from "./mapperFactories";
import {CloudFormationDescribeStacksResponse} from "./awsTypes";
import * as fs from "fs";
import {Vyne, SchemaImportRequest, SchemaSpec} from "@vyne/vyne-ts";
import {NameUtils} from "./names";
// import {
//     ServerlessFunction,
//     ServerlessOperationProviderFactory
// } from "@taxilang/taxi-typescript/src/operationGenerators/serverlessAwsLambdaGenerator";

export class VyneServerlessPlugin {

   commands: { [key: string]: any } = {};
   hooks: { [key: string]: Function };
   private provider: any;

   constructor(private serverless: ServerlessInstance, private options: any) {
      this.provider = this.serverless.getProvider(this.serverless.service.provider.name);
      this.hooks = {
         // 'after:deploy:finalize': this.generateTaxiDef.bind(this),
         // 'after:aws:info:displayEndpoints': this.generateTaxiDef.bind(this)
         'before:package:createDeploymentArtifacts': this.run.bind(this),
      }
   }

   private async getCloudFormationStack(): Promise<CloudFormationDescribeStacksResponse> {
      const StackName = this.provider.naming.getStackName(this.options.stage);
      return await this.provider.request('CloudFormation', 'describeStacks', {StackName}, this.options.stage, this.options.region);
   }

   async getServiceBaseUrl(): Promise<string> {
      this.serverless.cli.log("Requesting endpoint for taxi generation...");
      let stackDescription = await this.getCloudFormationStack();

      // let stackDescription: CloudFormationDescribeStacksResponse = await this.provider.request('CloudFormation', 'describeStacks', {StackName}, this.options.stage, this.options.region);
      if (stackDescription.Stacks.length == 0) {
         this.serverless.cli.log("Couldn't discover the base URL for the CloudFormation stack - nothing was returned.  Can't generate endpoints for functions in taxi def")
         return Promise.reject("Couldn't discover the base URL for the CloudFormation stack - nothing was returned.  Can't generate endpoints for functions in taxi def")
      }
      if (stackDescription.Stacks.length > 1) {
         return Promise.reject("Didn't expect multiple stacks back.")
      }
      let stack = stackDescription.Stacks[0];
      let serviceEndpoint = stack.Outputs.find(output => output.OutputKey === "ServiceEndpoint")
      if (!serviceEndpoint) {
         return Promise.reject("DescribeStacks response did not include ServiceEndpoint");
      }

      let serviceEndpointUrl = serviceEndpoint.OutputValue;
      this.serverless.cli.log(`Generating taxi with base endpoint of ${serviceEndpointUrl}`)

      return Promise.resolve(serviceEndpointUrl)
   }

   async run() {
      const taxi = await this.generateTaxiDef();

      let path = "./.serverless/schema.taxi";
      fs.writeFileSync(path, taxi);
      this.serverless.cli.log("Wrote taxi schema to " + path);
      let vyneConfig = this.serverless.service.custom["vyne"] as VynePluginConfig;
      if (vyneConfig && vyneConfig.url) {
         if (!vyneConfig.defaultNamespace) {
            this.serverless.cli.log("Warning: No namespace provided in Vyne config.  Will use the name of the service, but consider setting custom.vyne.defaultNamespace")
         }
         let namespace = (vyneConfig.defaultNamespace) ? vyneConfig.defaultNamespace : NameUtils.makeSafe(this.serverless.service.serviceObject.name);
         await this.registerSchema(vyneConfig.url, taxi, namespace)
      } else {
         this.serverless.cli.log("Not registering the generated taxi schema with Vyne, as no url was provided.  Consider adding config in serverless.yml at custom.vyne.url")
      }
   }

   async registerSchema(vyneUrl: string, schema: string, namespace: string) {
      let vyne = new Vyne(vyneUrl);
      let schemaName = this.serverless.service.serviceObject.name;
      let version = "0.1.0"; // TODO
      this.serverless.cli.log(`Registering ${schemaName}:${version} with Vyne at url ${vyneUrl}`);

      let request = new SchemaImportRequest(
         new SchemaSpec(schemaName, version,namespace),
         "taxi",
         schema
      );
      let versionedSchema = await vyne.schemaService.submitSchema(request);

      this.serverless.cli.log(`Schema ${versionedSchema.name}:${versionedSchema.version} registered successfully`);
   }

   async generateTaxiDef(): Promise<string> {

      // let vyneUrl =
      // return;
      // this.serverless.cli.log("HELLLOOOOOO");
      // this.serverless.cli.log("Generating taxi schema...");

      // let thisServerless = this.serverless as any;
      // this.serverless.cli.log("keys: " + Object.keys(this.serverless.service));
      // this.serverless.cli.log("Service:" + JSON.stringify(this.serverless.service.serviceObject));
      // this.serverless.cli.log("Resources: " + JSON.stringify(thisServerless.service.resources));

      let serviceUrl = await this.getServiceBaseUrl();
      // return;


      // this.serverless.cli.log("Serverless:")
      // this.serverless.cli.log(JSON.stringify(this.serverless));


      this.serverless.cli.log("Using source files:");
      this.rootFileNames.forEach(f => this.serverless.cli.log(f));

      const options = new SchemaGeneratorOptions();
      options.serviceMapperFactory = new ServerlessServiceMapperFactory(this.serverless.service);

      options.prependOperationProviderFactory(new ServerlessOperationProviderFactory(this.serverless.service, serviceUrl));
      const generator = new SchemaGenerator(options)
         .addSources(this.rootFileNames);

      try {
         const doc = generator.generate();
         this.serverless.cli.log(`Taxi contains ${doc.types.length} types and ${doc.services.length} services`);
         const taxiDef = new SchemaWriter().generateSchemas([doc]);

         // this.serverless.cli.log("Generated schemas:");?
         return taxiDef.join("\n");
      } catch (error) {
         this.serverless.cli.log("Error: " + error.message);
         this.serverless.cli.log(error.stack);
         throw error;
      }
   }

   get servicePath(): string {
      return this.serverless.config.servicePath
   }

   get rootFileNames(): string[] {
      return tsUtils.extractFileNames(this.servicePath, this.serverless.service.provider.name, this.functions)
   }

   get functions(): { [p: string]: ServerlessFunction } {
      return this.options.function
         ? {[this.options.function]: this.serverless.service.functions[this.options.function]}
         : this.serverless.service.functions
   }


}

module.exports = VyneServerlessPlugin;
