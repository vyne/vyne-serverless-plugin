import {
   Operation,
   OperationProvider,
   OperationProviderList,
   Service,
   ServiceMapper,
   TypeHelper,
   TypeMapper
} from "@taxilang/taxi-typescript";
import {ServerlessFunction, ServerlessService} from "./types";
import * as ts from 'typescript';
import {NameUtils} from "./names";

export class TypescriptHandlerResolver {
   private nodeToFunction = new Map<ts.Node, ServerlessFunction>();

   constructor(private serverlessConfig: ServerlessService, private typeHelper: TypeHelper) {
      Object.keys(this.serverlessConfig.functions).forEach(funName => {
         let fn = this.serverlessConfig.functions[funName];
         let operationDefinitionNode = this.typeHelper.findExportedNode(fn.handler);
         this.nodeToFunction.set(operationDefinitionNode, fn);
      });
   }


   getFunctionForNode(node: ts.Node): ServerlessFunction {
      let result = this.nodeToFunction.get(node);
      if (!result) {
         throw new Error("No handler matches provided  node")
      }
      return result;

   }
}

export class ServerlessServiceMapper implements ServiceMapper {
   private operationProvider: OperationProviderList;

   constructor(private serverlessConfig: ServerlessService, private typeHelper: TypeHelper, private typeMapper: TypeMapper, operationProviders: OperationProvider[]) {
      this.operationProvider = new OperationProviderList(operationProviders);
      this.services = this.build();
   }

   readonly services: Service[];

   private build(): Service[] {
      let operations: Operation[] = Object.keys(this.serverlessConfig.functions).map(funName => this.generateOperation(funName, this.serverlessConfig.functions[funName]));
      let service = <Service>{
         qualifiedName: NameUtils.makeSafe(this.serverlessConfig.serviceObject.name),
         operations: operations,
         annotations: []
      };
      return [service];
   }

   private generateOperation(functionName: string, fn: ServerlessFunction): Operation {
      let operationDefinitionNode = this.typeHelper.findExportedNode(fn.handler);
      if (!this.operationProvider.canProvideFor(operationDefinitionNode)) {
         throw new Error(`No operation mapper found that can build operation for ${functionName}`)
      } else {
         return this.operationProvider.getOperation(operationDefinitionNode)
      }
   }

}
