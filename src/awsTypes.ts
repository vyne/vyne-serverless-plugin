
/**
 * models the response from a cloudFormation describeStacks request.
 * Other properties exist, only mapped some.
 */
export interface CloudFormationDescribeStacksResponse {
   Stacks: CloudFormationStack[]
}

export interface CloudFormationStack {
   StackId: string;
   StackName: string;
   Description: string;
   CreationTime: string;
   LastUpdatedTime: string;
   Outputs: CloudFormationStackOutput[]
}

export interface CloudFormationStackOutput {
   OutputKey: string
   OutputValue: string
   Description: string | undefined
}
