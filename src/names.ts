export class NameUtils {

   static makeSafe(name:string):string {
      return name.replace(/[^a-zA-Z0-9]/,"");
   }
}
