import * as ts from "typescript";
import {Handler} from "aws-lambda";
import {
   Annotation,
   Operation,
   OperationProvider,
   OperationProviderFactory,
   Parameter,
   Primitives,
   TypeHelper,
   TypeMapper
} from "@taxilang/taxi-typescript";
import {ServerlessEventConfig, ServerlessEventType, ServerlessFunction, ServerlessService} from "./types";
import {HttpOperationContext, ServerlessEventAnnotationMappers} from "./eventMappers";
import * as _ from 'lodash';
import {TypescriptHandlerResolver} from "./serviceMapper";

export interface BaseUrlWrapper<T extends ServerlessEventConfig> {
   baseUrl: string
   config: T

}

export type AwsHttpEventWithBaseUrl = BaseUrlWrapper<AwsHttpEventConfig>

export interface AwsHttpEventConfig extends ServerlessEventConfig {
   path: string;
   method: string;
   pathVariableTypes: { [key: string]: string }
}

export class ServerlessOperationProviderFactory implements OperationProviderFactory {
   constructor(private serverlessConfig: ServerlessService, private serviceBaseUrl: string) {

   }

   build(typeHelper: TypeHelper, typeMapper: TypeMapper): OperationProvider[] {
      return <OperationProvider[]>[
         new OperationMapper(new TypescriptHandlerResolver(this.serverlessConfig, typeHelper), typeHelper, typeMapper, this.serviceBaseUrl),
      ]
   }
}

export class OperationMapper implements OperationProvider {
   private annotationMapper = new ServerlessEventAnnotationMappers();

   constructor(private handlerResolver: TypescriptHandlerResolver, private typeHelper: TypeHelper, private typeMapper: TypeMapper, private baseUrl: string) {
   }

   canProvideFor(node: ts.Node): boolean {
      if (!ts.isVariableStatement(node) && !ts.isVariableDeclaration(node)) {
         return false;
      } else {
         let declaration = this.getVariableDeclaration(node);
         let type = declaration.type as ts.Node;
         let name = this.typeHelper.getFullyQualifiedName(type);
         return name.nameLooksCloseTo("@types/aws-lambda", "Handler");
      }
   }

   getOperation(node: ts.Node): Operation {
      if (!ts.isVariableStatement(node) && !ts.isVariableDeclaration(node)) {
         throw new Error("Invalid type of node - expected variableStatement or VariableDeclaration")
      }
      let declaration = this.getVariableDeclaration(node);
      let fn: ServerlessFunction = this.handlerResolver.getFunctionForNode(node);
      let declaredType = declaration.type;
      let returnType;
      if (declaredType && ts.isTypeReferenceNode(declaredType)) {
         if ((declaredType.typeArguments || []).length == 2) {
            returnType = this.typeMapper.getTypeOrDefault(declaredType.typeArguments![1], Primitives.ANY)
         } else {
            returnType = Primitives.ANY
         }
      } else {
         returnType = Primitives.ANY
      }

      let annotations = this.generateAnnotationsFromFunction(fn);
      let parameters = this.parseParams(declaration);
      let operationName = this.typeHelper.getNameFromIdentifier(declaration.name as ts.Identifier);
      return <Operation>{
         name: operationName,
         scope: null, // TODO
         returnType: returnType,
         contract: null,
         annotations: annotations, // TODO,
         parameters: parameters
      };
   }

   private parseParams(declaration: ts.VariableDeclaration): Parameter[] {
      return []; // TODO
   }

   private getVariableDeclaration(node: ts.VariableStatement | ts.VariableDeclaration): ts.VariableDeclaration {
      if (ts.isVariableDeclaration(node)) {
         return node
      } else if (ts.isVariableStatement(node)) {
         if (node.declarationList.declarations.length > 1) {
            // This is probably ok, but I want to understand when this would occur
            throw new Error("Multiple declarations on a variable statement are not yet handled.  Investigate this.")
         }
         return node.declarationList.declarations[0];
      } else {
         throw new Error("Unhandled branch")
      }

   }

   private generateAnnotationsFromFunction(fn: ServerlessFunction): Annotation[] {
      let context = <HttpOperationContext>{
         baseUrl: this.baseUrl
      };
      let annotations = _.flatMap(fn.events, (eventConfigEntry) => {
         return _.flatMap(Object.keys(eventConfigEntry), event => {
            let eventType = event as ServerlessEventType;
            let config = eventConfigEntry[event] as ServerlessEventConfig;
            return this.annotationMapper.build(eventType, config, context)
         })
      });
      return annotations;
   }
}
