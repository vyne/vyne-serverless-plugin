import {AwsHttpEventConfig} from "./operationMapper";
import {Annotation} from "@taxilang/taxi-typescript";
import {ServerlessEventConfig, ServerlessEventType} from "./types";

/**
 * Stuff that operation mappers need to know to build an operation (or part of it),
 * but is only knowable at build time.
 * eg: baseUrl of the service / address of the lambda / event queue urls, etc
 */
export interface OperationBuildContext {

}

export interface HttpOperationContext extends OperationBuildContext {
   baseUrl: string
}

export class ServerlessEventAnnotationMappers {
   private mappers: ServerlessEventAnnotationMapper<any, any>[] = [
      new ApiGatewayRequestAnnotationMapper()
   ];

   build(eventType: ServerlessEventType, config: ServerlessEventConfig, operationContext: OperationBuildContext): Annotation[] {
      let mapper = this.mappers.find(m => m.supportedType == eventType);
      if (!mapper) {
         throw new Error("No annotation mapper defined to support event type " + eventType)
      }
      return mapper.build(config, operationContext);
   }
}

export interface ServerlessEventAnnotationMapper<T extends ServerlessEventConfig, C extends OperationBuildContext> {
   supportedType: ServerlessEventType

   build(config: T, context: C): Annotation[];
}

export class ApiGatewayRequestAnnotationMapper implements ServerlessEventAnnotationMapper<AwsHttpEventConfig, HttpOperationContext> {
   get supportedType(): ServerlessEventType {
      return ServerlessEventType.http;
   }

   static replaceVariablesWithTypeTokens(path: string, variableTypes: { [key: string]: string }): string {
      if (!variableTypes) {
         throw new Error("VariableTypes must be provided")
      }
      return path.replace(/[^{\}]+(?=})/g, (match, substring) => {
         let replacement = variableTypes[match];
         if (!replacement) {
            throw new Error(`Path "${path}" defines variable "${match}", but config does not define a type for it.  Ensure that you have a "pathVariableTypes" block in your serverless for this function under events.http.pathVariableTypes.${match}`)
         }
         return replacement
      });
   }

   build(config: AwsHttpEventConfig, context: HttpOperationContext): Annotation[] {
      // TODO : Should type-check that the type mapped actually exists
      console.log("Trying to map " + config.path);
      const relativeUrl = ApiGatewayRequestAnnotationMapper.replaceVariablesWithTypeTokens(config.path, config.pathVariableTypes || {});
      const absoluteUrl = this.joinWithSlash(context.baseUrl, relativeUrl);
      let annotation = <Annotation>{
         name: "HttpOperation",
         parameters: {
            "method": config.method.toUpperCase(),

            "url": absoluteUrl
         }
      };
      console.log("Built annotation: " + JSON.stringify(annotation))
      return [annotation]
   }

   private joinWithSlash(baseUrl: string, relativeUrl: string): string {
      const baseUrlWithTrailingSlash = (baseUrl.endsWith("/")) ? baseUrl : baseUrl + "/";
      const relativeUrlWithoutPrecedingSlash = (relativeUrl.startsWith("/")) ? relativeUrl.substr(1) : relativeUrl;
      return baseUrlWithTrailingSlash + relativeUrlWithoutPrecedingSlash
   }
}
