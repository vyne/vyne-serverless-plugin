export interface ServerlessInstance {
   cli: {
      log(str: string): void
   }
   config: {
      servicePath: string
   }

   service: ServerlessService
   pluginManager: PluginManager

   getProvider(name: string): ServerlessProvider
}

export interface VynePluginConfig {
   url : string
   defaultNamespace: string | undefined
}
export interface ServerlessProvider {

}

export interface ServerlessService {
   serviceObject: {
      name: string
   }
   custom: any | undefined,
   provider: {
      name: string
   }
   functions: { [key: string]: ServerlessFunction }
   package: ServerlessPackage
   getAllFunctions: () => string[]
}

// export type ServerlessEventType = 'http' // others to be supported
// export type AwsEventType = 'http' // Others to be supported
export interface ServerlessEventConfig {

}

export enum ServerlessEventType {
   http = "http"
}

export interface ServerlessEventTypeConfigEntry {
   [key: string]: ServerlessEventConfig
}

export interface ServerlessFunction {
   handler: string
   events: ServerlessEventTypeConfigEntry[]
}


export interface ServerlessPackage {
   include: string[]
   exclude: string[]
   artifact?: string
   individually?: boolean
}

export interface PluginManager {
   spawn(command: string): Promise<void>
}

