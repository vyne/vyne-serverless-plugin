import {ApiGatewayRequestAnnotationMapper} from "./eventMappers";
import {expect} from "chai";

describe("ApiGatewayRequestAnnotationMapper", () => {
   describe("parsing types from path", () => {
      it("should replace variables with type tokens", () => {
         let replaced = ApiGatewayRequestAnnotationMapper.replaceVariablesWithTypeTokens("/hello/{name}", {"name" : "foo.ClientName"})
         expect(replaced).to.equal("/hello/{foo.ClientName}")
      })
   })
})
